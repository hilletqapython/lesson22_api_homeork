import pytest

from lesson22_homework_REST_API.api.entities.planet_object import PlanetInfo
from lesson22_homework_REST_API.api.infrastructure.planet import Planet


@pytest.fixture()
def planets():
    yield Planet()


@pytest.fixture()
def alderaan():
    yield PlanetInfo(name="Alderaan",
                     rotation_period="24",
                     orbital_period="364",
                     diameter="12500",
                     climate="temperate",
                     gravity="1 standard",
                     terrain="grasslands, mountains",
                     surface_water="40",
                     population="2000000000")

