class PlanetInfo:
    """
    Describes planet information
    """
    def __init__(self, name: str,
                 rotation_period: str,
                 orbital_period: str,
                 diameter: str,
                 climate: str,
                 gravity: str,
                 terrain: str,
                 surface_water: str,
                 population: str):
        self.name = name
        self.rotation_period = rotation_period
        self.orbital_period = orbital_period
        self.diameter = diameter
        self.climate = climate
        self.gravity = gravity
        self.terrain = terrain
        self.surface_water = surface_water
        self.population = population

    def __eq__(self, other):
        """
        Check if stats of a planet that we describe in PlanetInfo
        are the same as of response planet
        Args:
            other:
                (Planet) planet from response
        Returns:
                (Bool) True or False
        """
        return self.__dict__ == other.__dict__




















