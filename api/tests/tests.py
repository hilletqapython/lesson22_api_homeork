from lesson22_homework_REST_API.api.entities.planet_object import PlanetInfo


def test_name(planets):
    response = planets.get(2)
    assert response.json()["name"] == "Alderaan"


def test_is_key_present(planets):
    response = planets.get(1)
    keys = ["diameter", "terrain", "population"]
    assert all(key in response.json() for key in keys)


def test_population(planets):
    response = planets.get(2)
    assert response.json()["population"] == "2000000000"


def test_planet(planets, alderaan):
    response = planets.get(2)
    print(type(planets.get(2)))
    planet_for_test = PlanetInfo(
        response.json()["name"],
        response.json()["rotation_period"],
        response.json()["orbital_period"],
        response.json()["diameter"],
        response.json()["climate"],
        response.json()["gravity"],
        response.json()["terrain"],
        response.json()["surface_water"],
        response.json()["population"],
        )
    assert planet_for_test == alderaan

