from requests import get

from lesson22_homework_REST_API.api.config import config


class Planet:
    """
    Plane that we will get from response in test
    """
    def __init__(self):
        self.url = f'{config["url"]}/planets/'

    def get(self, planet_id):
        """
        Function to get planet from database
        Args:
            planet_id:
                (int) id of planet in database
        Returns:
                (HTTP) requested planet
        """
        return get(f"{self.url}/{planet_id}")

